/**
 * @file hachi_nn.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief
 * @version 0.1.0
 * @date 2021-12-16
 *
 * @copyright Copyright (c) 2021
 *
 */
#ifndef __ETHER_HACHI_NN__
#define __ETHER_HACHI_NN__

#include <ether_activ_func.h>
#include <ether_neuron.h>

#ifdef __cplusplus
extern "C"
{
#endif

    typedef struct _hachi_nn HachiNNModel;

    /**
     * @brief Create new ANN model
     *
     * @param numb_input_neuron Number of neurons in input layer (e.g. 2)
     * @param numb_output_neuron Number of neurons in output layer (e.g. 1)
     * @param num_hidden_layer Number of hidden layer (e.g. 3)
     * @param hidden_layer_config Number of neurons in each hidden layer (e.g. [2,3,2])
     * @return HachiNNModel*
     */
    HachiNNModel *hachi_nn_model_new(unsigned int numb_input_neuron, unsigned int numb_output_neuron, unsigned int num_hidden_layer, unsigned int *hidden_layer_config);

    void hachi_nn_set_input_layer(HachiNNModel *model_nn, Neuron **layer);
    Neuron **hachi_nn_get_input_layer(HachiNNModel *medoel_nn);
    void hachi_nn_set_output_layer(HachiNNModel *model_nn, Neuron **layer);
    Neuron **hachi_nn_get_output_layer(HachiNNModel *model_nn);
    void hachi_nn_set_hidden_layer(HachiNNModel *model_nn, Neuron ***layer_pack);
    Neuron ***hachi_nn_get_hidden_layer(HachiNNModel *model_nn);
    void hachi_nn_set_hidden_layer_at_index(HachiNNModel *model_nn, Neuron **layer, unsigned int pos_layer);
    Neuron **hachi_nn_get_hidden_layer_at_index(HachiNNModel *model_nn, unsigned int pos_layer);
    void hachi_nn_set_activation_function_to_layer(Neuron **layer, unsigned int numb_neuron_in_layer, ActivateFunction activate_func);
    void hachi_nn_set_activation_function_to_hidden_layer(HachiNNModel *model_nn, ActivateFunction act_func);
    void hachi_nn_set_activation_function_to_hidden_layer_at_index(HachiNNModel *model_nn, unsigned int pos_layer, ActivateFunction act_func);
    void hachi_nn_set_activation_function_to_output_layer(HachiNNModel *model_nn, ActivateFunction act_func);
    void hachi_nn_set_bias_to_layer(Neuron **layer, unsigned int numb_neuron_in_layer, double bias_val);
    void hachi_nn_set_bias_to_hidden_layer(HachiNNModel *model_nn, double bias_val);
    void hachi_nn_set_bias_to_hidden_layer_at_index(HachiNNModel *model_nn, unsigned int pos_layer, double bias_val);
    void hachi_nn_set_bias_to_output_layer(HachiNNModel *model_nn, double bias_val);

#ifdef __cplusplus
}
#endif

#endif /*__ETHER_HACHI_NN__*/