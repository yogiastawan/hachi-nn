/**
 * @file hachi_nn.c
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief
 * @version 0.1.0
 * @date 2021-12-16
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <hachi_nn.h>
#include <stdio.h>
#include <stdlib.h>

struct _hachi_nn
{
    unsigned int numbInputNeuron;
    unsigned int numOutputNeuron;
    unsigned int numbHiddenLayer;
    unsigned int *hiddenLayerConfig;
    Neuron **inputLayer;
    Neuron **outputLayer;
    Neuron ***hiddenLayer;
};

HachiNNModel *hachi_nn_model_new(unsigned int numb_input_neuron, unsigned int numb_output_neuron, unsigned int num_hidden_layer, unsigned int *hidden_layer_config)
{
    HachiNNModel *model = (HachiNNModel *)malloc(sizeof(HachiNNModel));
    model->numbInputNeuron = numb_input_neuron;
    model->numOutputNeuron = numb_output_neuron;
    model->numbHiddenLayer = num_hidden_layer;
    model->hiddenLayerConfig = (unsigned int *)malloc(sizeof(int) * num_hidden_layer);
    model->hiddenLayerConfig = hidden_layer_config;
    // input layer
    model->inputLayer = (Neuron **)malloc(sizeof(Neuron) * numb_input_neuron);
    // create neurons with fully connected
    unsigned int i = 0;
    for (i = 0; i < numb_input_neuron; i++)
    {
        Neuron *tmp = neuron_new(((num_hidden_layer > 0) * hidden_layer_config[0]) + ((num_hidden_layer <= 0) * numb_output_neuron), i);
        model->inputLayer[i] = tmp;
    }
    // output layer
    model->outputLayer = (Neuron **)malloc(sizeof(Neuron) * numb_output_neuron);
    // create neurons with fully connected
    for (i = 0; i < numb_output_neuron; i++)
    {
        Neuron *tmp = neuron_new(0, i);
        model->outputLayer[i] = tmp;
    }
    // hidden layer
    model->hiddenLayer = (Neuron ***)malloc(sizeof(Neuron) * num_hidden_layer);
    // create neurons with fully connected
    for (i = 0; i < num_hidden_layer; i++)
    {
        model->hiddenLayer[i] = (Neuron **)malloc(sizeof(Neuron) * hidden_layer_config[i]);
        unsigned int j = 0;
        for (j = 0; j < hidden_layer_config[i]; j++)
        {
            Neuron *tmp = neuron_new(((i < (num_hidden_layer - 1) * hidden_layer_config[i + 1]) + ((i >= (num_hidden_layer - 1)) * numb_output_neuron)), j);
            model->hiddenLayer[i][j] = tmp;
        }
    }

    return model;
}

void hachi_nn_set_input_layer(HachiNNModel *model_nn, Neuron **layer)
{
    // free memory allocation in initialization first
    // and set new layer
    unsigned int i = 0;
    for (i = 0; i < model_nn->numbInputNeuron; i++)
    {
        free(model_nn->inputLayer[i]);
    }
    free(model_nn->inputLayer);
    model_nn->inputLayer = layer;
}

Neuron **hachi_nn_get_input_layer(HachiNNModel *medoel_nn)
{
    return medoel_nn->inputLayer;
}

void hachi_nn_set_output_layer(HachiNNModel *model_nn, Neuron **layer)
{
    // free memory allocation in initialization first
    // and set new layer
    unsigned int i = 0;
    for (i = 0; i < model_nn->numOutputNeuron; i++)
    {
        free(model_nn->outputLayer[i]);
    }
    free(model_nn->outputLayer);
    model_nn->outputLayer = layer;
}

Neuron **hachi_nn_get_output_layer(HachiNNModel *model_nn)
{
    return model_nn->outputLayer;
}

void hachi_nn_set_hidden_layer(HachiNNModel *model_nn, Neuron ***layer_pack)
{
    // free memory allocation in initialization first
    // and set new layer
    unsigned int i = 0;
    for (i = 0; i < model_nn->numbHiddenLayer; i++)
    {
        unsigned int j = 0;
        for (j = 0; j < model_nn->hiddenLayerConfig[i]; j++)
        {
            free(model_nn->hiddenLayer[i][j]);
        }
        free(model_nn->hiddenLayer[i]);
    }
    free(model_nn->hiddenLayer);
    model_nn->hiddenLayer = layer_pack;
}

Neuron ***hachi_nn_get_hidden_layer(HachiNNModel *model_nn)
{
    return model_nn->hiddenLayer;
}

void hachi_nn_set_hidden_layer_at_index(HachiNNModel *model_nn, Neuron **layer, unsigned int pos_layer)
{
    // free memory allocation in initialization first
    // and set new layer
    unsigned int i = 0;
    for (i = 0; i < model_nn->hiddenLayerConfig[pos_layer]; i++)
    {
        free(model_nn->hiddenLayer[pos_layer][i]);
    }
    free(model_nn->hiddenLayer[pos_layer]);
    model_nn->hiddenLayer[pos_layer] = layer;
}

Neuron **hachi_nn_get_hidden_layer_at_index(HachiNNModel *model_nn, unsigned int pos_layer)
{
    return model_nn->hiddenLayer[pos_layer];
}

void hachi_nn_set_activation_function_to_layer(Neuron **layer, unsigned int numb_neuron_in_layer, ActivateFunction activate_func)
{
    int i = 0;
    for (i = 0; i < numb_neuron_in_layer; i++)
    {
        neuron_set_activation_function(layer[i], activate_func);
        // layer[i]->activate_func = activate_func;
    }
}

void hachi_nn_set_activation_function_to_hidden_layer(HachiNNModel *model_nn, ActivateFunction act_func)
{
    unsigned int i = 0;
    for (i = 0; i < model_nn->numbHiddenLayer; i++)
    {
        unsigned int j = 0;
        for (j = 0; j < model_nn->hiddenLayerConfig[i]; j++)
        {
            // model_nn->hiddenLayer[i][j]->activate_func=act_func;
            neuron_set_activation_function(model_nn->hiddenLayer[i][j], act_func);
        }
    }
}

void hachi_nn_set_activation_function_to_hidden_layer_at_index(HachiNNModel *model_nn, unsigned int pos_layer, ActivateFunction act_func)
{
    unsigned int i = 0;
    for (i = 0; i < model_nn->hiddenLayerConfig[pos_layer]; i++)
    {
        // model_nn->hiddenLayer[pos_layer][i]->activate_func=act_func;
        neuron_set_activation_function(model_nn->hiddenLayer[pos_layer][i], act_func);
    }
}

void hachi_nn_set_activation_function_to_output_layer(HachiNNModel *model_nn, ActivateFunction act_func)
{
    unsigned int i = 0;
    for (i = 0; i < model_nn->numOutputNeuron; i++)
    {
        neuron_set_activation_function(model_nn->outputLayer[i], act_func);
    }
}

void hachi_nn_set_bias_to_layer(Neuron **layer, unsigned int numb_neuron_in_layer, double bias_val)
{
    unsigned int i = 0;
    for (i = 0; i < numb_neuron_in_layer; i++)
    {
        neuron_set_bias(layer[i], bias_val);
    }
}

void hachi_nn_set_bias_to_hidden_layer(HachiNNModel *model_nn, double bias_val)
{
    unsigned int i = 0;
    for (i = 0; i < model_nn->numbHiddenLayer; i++)
    {
        unsigned int j = 0;
        for (j = 0; j < model_nn->hiddenLayerConfig[i]; j++)
        {
            neuron_set_bias(model_nn->hiddenLayer[i][j], bias_val);
        }
    }
}

void hachi_nn_set_bias_to_hidden_layer_at_index(HachiNNModel *model_nn, unsigned int pos_layer, double bias_val)
{
    unsigned int i = 0;
    for (i = 0; i < model_nn->hiddenLayerConfig[pos_layer]; i++)
    {
        neuron_set_bias(model_nn->hiddenLayer[pos_layer][i]);
    }
}

void hachi_nn_set_bias_to_output_layer(HachiNNModel *model_nn, double bias_val)
{
    unsigned int i = 0;
    for (i = 0; i < model_nn->numOutputNeuron; i++)
    {
        neuron_set_bias(model_nn->outputLayer[i], bias_val);
    }
}
