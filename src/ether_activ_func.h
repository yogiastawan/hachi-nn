/**
 * @file ether_activ_func.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief
 * @version 0.1.0
 * @date 2021-12-16
 *
 * @copyright Copyright (c) 2021
 *
 */
#ifndef __ETHER_ACTIVATION_FUNCTION_NN__
#define __ETHER_ACTIVATION_FUNCTION_NN__

#ifdef __cplusplus
extern "C"
{
#endif

    typedef enum activate_func
    {
        ACT_FUNC_NONE, ///< For input layer only
        ACT_FUNC_BINARI_STEP,
        ACT_FUNC_LINEAR,
        ACT_FUNC_SIGMOID,
        ACT_FUNC_SIGMOID_DERIVATIVE,
        ACT_FUNC_TANH,
        ACT_FUNC_TANH_DERIVATIVE,
        ACT_FUNC_RETIFIED_LINEAR,
        ACT_FUNC_LEAKY_RECTIFIED_LINEAR,
        ACT_FUNC_LEAKY_RECTIFIED_LINEAR_DERIVATIVE,
        ACT_FUNC_PARAMETRIC_RECTIFIED_LINEAR,///< Make sure to set alpha in neuron first.
        ACT_FUNC_EXPONENTIAL_RECTIFIED_LINEAR,///< Make sure to set alpha in neuron first.
        ACT_FUNC_EXPONENTIAL_RECTIFIED_LINEAR_DERIVATIVE,///< Make sure to set alpha in neuron first.
        ACT_FUNC_SOFTMAX,
        ACT_FUNC_SWISH,
        ACT_FUNC_GAUSSIAN_ERROR_LINEAR,
        ACT_FUNC_SCALED_EXPONENTIAL_LINEAR,///< Make sure to set alpha and lamda in neuron first.
        ACT_FUNC_CUSTOM
    } ActivateFunction;

    double func_binary_step(double input);
    double func_linear(double input);
    double func_sigmoid(double input);
    double func_sigmoid_derivative(double input);
    double func_hyperbolic_tan(double input);
    double func_hyperbolic_tan_derivative(double input);
    double func_rectified_linear(double input);
    double func_leaky_rectified_linear(double input);
    double func_leaky_rectified_linear_derivative(double input);
    double func_parametric_rectified_linear(double input, double negative_slope);
    double func_exponential_rectified_linear(double input, double negative_slope);
    double func_exponential_rectified_linear_derivative(double input, double negative_slope);
    double func_softmax(double *input, unsigned int length_input, unsigned int index);
    double func_swish(double input);
    double func_gaussian_error_linear(double input);
    double func_scaled_exponential_linear(double input, double alpha, double lamda);

#ifdef __cplusplus
}
#endif

#endif /*__ETHER_ACTIVATION_FUNCTION_NN__*/