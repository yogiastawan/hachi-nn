/**
 * @file ether_neuron.h
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief
 * @version 0.1.0
 * @date 2021-12-16
 *
 * @copyright Copyright (c) 2021
 *
 */
#ifndef __ETHER_NEURON_NN__
#define __ETHER_NEURON_NN__

#include <ether_activ_func.h>

#ifdef __cplusplus
extern "C"
{
#endif

    typedef struct _neuron Neuron;
    struct _neuron
    {
        int index;
        double val;
        double bias;
        ActivateFunction activate_func;
        int numb_weight;
        double *weight;
        double alpha;
        double lamda;
        // int numb_prev_neuron_connected;//-1 if all connected
        // int *prev_neuron_connected;//NULL if all connected
    };

    /**
     * @brief Create new neuron
     *
     * @param numb_weight Number of weight neuron
     * @param index_in_layer Index position of neuron in layer
     * @return Neuron*
     */
    Neuron *neuron_new(int numb_weight, int index_in_layer);

    /**
     * @brief Calculate the value of neuron
     *
     * @param neuron The neuron to be calculated
     * @param prev_layer The previous layer
     * @param numb_prev_layer number of neurons in previous layer
     */
    void neuron_calc_val(Neuron *neuron, Neuron **prev_layer, int numb_prev_layer);

    /**
     * @brief Calculate value of neuron with softmax. Use it only when all neurons in current layer is calculated with neuron_calc_val
     *
     * @param neuron_layer Current neuron layer
     * @param numb_neuron_in_layer Number of neurons in current layer
     */
    void neuron_calc_val_softmax(Neuron **neuron_layer, int numb_neuron_in_layer);

    /**
     * @brief Set value of neuron
     *
     * @param neuron The neuron to be value changed
     * @param val The new value
     */
    void neuron_set_val(Neuron *neuron, double val);

    /**
     * @brief Get value of the neuron
     *
     * @param neuron The neuron
     * @return double
     */
    double neuron_get_val(Neuron *neuron);
    void neuron_set_weight(Neuron *neuron, int index_weight, double weigth_val);
    void neuron_get_weight(Neuron *neuron, int index);
    void neuron_set_bias(Neuron *neuron, double bias_val);
    double neuron_get_bias(Neuron *neuron);
    void neuron_set_activation_function(Neuron *neuron, ActivateFunction act_func);
    ActivateFunction neuron_get_activation_function(Neuron *neuron);

#ifdef __cplusplus
}
#endif

#endif /*__ETHER_NEURON_NN__*/