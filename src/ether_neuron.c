/**
 * @file ether_neuron.c
 * @author yogiastawan (yogi.astawan@gmail.com)
 * @brief
 * @version 0.1.0
 * @date 2021-12-16
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <ether_neuron.h>
#include <stdlib.h>

static double random_val()
{
    return (double)rand() / (double)RAND_MAX;
}


Neuron *neuron_new(int numb_weight, int index_in_layer)
{
    Neuron *neuron = (Neuron *)malloc(sizeof(Neuron));
    neuron->index = index_in_layer;
    neuron->val = 0;
    neuron->bias = 0;
    neuron->activate_func = ACT_FUNC_NONE;
    neuron->numb_weight = numb_weight;
    neuron->weight = (double *)malloc(sizeof(double) * numb_weight);
    int i = 0;
    for (i = 0; i < numb_weight; i++)
    {
        neuron->weight[i] = random_val();
    }
    neuron->alpha = 0;
    neuron->lamda = 0;

    return neuron;
}

void neuron_calc_val(Neuron *neuron, Neuron **prev_layer, int numb_prev_layer)
{
    double val = neuron->bias;
    // calculate all previous neuron layer
    int i = 0;
    for (i = 0; i < numb_prev_layer; i++)
    {
        Neuron *temp_neuron = prev_layer[i];
        val += temp_neuron->val * temp_neuron->weight[neuron->index];
    }

    // activation function
    double val_out = 0;
    switch (neuron->activate_func)
    {
    case ACT_FUNC_NONE:
        val_out = val;
        break;
    case ACT_FUNC_BINARI_STEP:
        val_out = func_binary_step(val);
        break;
    case ACT_FUNC_LINEAR:
        val_out = func_linear(val);
        break;
    case ACT_FUNC_SIGMOID:
        val_out = func_sigmoid(val);
        break;
    case ACT_FUNC_SIGMOID_DERIVATIVE:
        val_out = func_sigmoid_derivative(val);
        break;
    case ACT_FUNC_TANH:
        val_out = func_hyperbolic_tan(val);
        break;
    case ACT_FUNC_TANH_DERIVATIVE:
        val_out = func_hyperbolic_tan_derivative(val);
        break;
    case ACT_FUNC_RETIFIED_LINEAR:
        val_out = func_rectified_linear(val);
        break;
    case ACT_FUNC_LEAKY_RECTIFIED_LINEAR:
        val_out = func_leaky_rectified_linear(val);
        break;
    case ACT_FUNC_LEAKY_RECTIFIED_LINEAR_DERIVATIVE:
        val_out = func_leaky_rectified_linear_derivative(val);
        break;
    case ACT_FUNC_PARAMETRIC_RECTIFIED_LINEAR:
        val_out = func_parametric_rectified_linear(val, neuron->alpha);
        break;
    case ACT_FUNC_EXPONENTIAL_RECTIFIED_LINEAR:
        val_out = func_exponential_rectified_linear(val, neuron->alpha);
        break;
    case ACT_FUNC_EXPONENTIAL_RECTIFIED_LINEAR_DERIVATIVE:
        val_out = func_exponential_rectified_linear_derivative(val, neuron->alpha);
        break;
    case ACT_FUNC_SOFTMAX:
        val_out = val;
        break;
    case ACT_FUNC_SWISH:
        val_out = func_swish(val);
        break;
    case ACT_FUNC_GAUSSIAN_ERROR_LINEAR:
        val_out = func_gaussian_error_linear(val);
        break;
    case ACT_FUNC_SCALED_EXPONENTIAL_LINEAR:
        val_out = func_scaled_exponential_linear(val, neuron->alpha, neuron->lamda);
        break;
    default:
        break;
    }

    neuron->val = val_out;
}

// call it when all neuron in current layer is calcuated
void neuron_calc_val_softmax(Neuron **neuron_layer, int numb_neuron_in_layer)
{
    double *val = (double *)malloc(sizeof(double) * numb_neuron_in_layer);
    int i = 0;
    for (i = 0; i < numb_neuron_in_layer; i++)
    {
        val[i] = neuron_layer[i]->val;
    }
    for (i = 0; i < numb_neuron_in_layer; i++)
    {
        neuron_layer[i]->val = func_softmax(val, numb_neuron_in_layer, i);
    }
}

void neuron_set_val(Neuron *neuron, double val)
{
    neuron->val = val;
}

double neuron_get_val(Neuron *neuron)
{
    return neuron->val;
}

void neuron_set_weight(Neuron *neuron, int index_weight, double weigth_val)
{
    neuron->weight[index_weight] = weigth_val;
}

void neuron_get_weight(Neuron *neuron, int index)
{
    return neuron->weight[index];
}
void neuron_set_bias(Neuron *neuron, double bias_val)
{
    neuron->bias = bias_val;
}

double neuron_get_bias(Neuron *neuron)
{
    return neuron->bias;
}

void neuron_set_activation_function(Neuron *neuron, ActivateFunction act_func)
{
    neuron->activate_func = act_func;
}
ActivateFunction neuron_get_activation_function(Neuron *neuron)
{
    return neuron->activate_func;
}