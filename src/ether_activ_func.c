#include <ether_activ_func.h>
#include <math.h>

static double sum_exp(double *data, unsigned int length);

double func_binary_step(double input)
{
    return (input >= 0.0);
}

double func_linear(double input)
{
    return input;
}

double func_sigmoid(double input)
{
    return 1.0 / (1.0 + exp(-input));
}

double func_sigmoid_derivative(double input)
{
    double sigmoid_val = func_sigmoid(input);
    return sigmoid_val * (1.0 - sigmoid_val);
}

double func_hyperbolic_tan(double input)
{
    // return (exp(input) - exp(-input)) / (exp(input) + exp(-input));
    return tanh(input);
}

double func_hyperbolic_tan_derivative(double input)
{
    double tanhx = tanh(input);
    return 1.0 - pow(tanhx, 2.0);
}

double func_rectified_linear(double input)
{
    return (input >= 0.0) * input;
}

double func_leaky_rectified_linear(double input)
{
    double n_val = 0.1 * input;
    return ((n_val > input) * n_val) + ((n_val <= input) * input);
}

double func_leaky_rectified_linear_derivative(double input)
{
    return ((input < 0.0) * 0.01) + (input >= 0.0);
}

double func_parametric_rectified_linear(double input, double negative_slope)
{
    double n_val = negative_slope * input;
    return ((n_val > input) * n_val) + ((n_val <= input) * input);
}

double func_exponential_rectified_linear(double input, double negative_slope)
{
    double n_val = negative_slope * (exp(input) - 1.0);
    return ((input < 0.0) * n_val) + ((input >= 0.0) * input);
}

double func_exponential_rectified_linear_derivative(double input, double negative_slope)
{
    double n_val = (negative_slope * (exp(input) - 1.0)) + negative_slope;
    return ((input < 0.0) * n_val) + ((input >= 0.0));
}

double func_softmax(double *input, unsigned int length_input, unsigned int index)
{
    return exp(input[index])/sum_exp(input,length_input);
}

double func_swish(double input)
{
    return input * func_sigmoid(input);
}

double func_gaussian_error_linear(double input)
{
    double sqr = sqrt(2.0 / M_PI);
    double val = 0.044715 * pow(input, 3.0);
    double tanhx = tanh(sqr * (input + val));
    return input * (1.0 + tanhx) / 2.0;
}

double func_scaled_exponential_linear(double input, double alpha, double lamda)
{
    double n_val = alpha * (exp(input) - 1.0);
    return lamda * (((input < 0.0) * n_val) + ((input >= 0.0) * input));
}

static double sum_exp(double *data, unsigned int length)
{
    double val = 0;
    int i = 0;
    for (i = 0; i < length; i++)
    {
        val += exp(data[i]);
    }
    return val;
}